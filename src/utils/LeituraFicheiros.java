/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import model.Cidadao;
import model.Reparticao;
import model.Cidadao.CodigoPostal;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;
import model.RegistoReparticao;
import model.Servico;

/**
 * Classe utilitária para proceder à leitura de ficheiros.
 *
 * @author Gonçalo Martins 1161544
 * @author João Dias 1160967
 */
public class LeituraFicheiros {

    /**
     * Mensagem de erro que é apresentada ao utilizador caso haja números inválidos nos ficheiros, como, por exemplo, 1A2.
     */
    private static final String MENSAGEM_ERRO_NUMEROS = "Verifique o conteúdo do ficheiro, foram encontrados números inválidos!";

    /**
     * Lê um ficheiro de texto com as informações das repartições e coloca em memória essas informações.
     *
     * @param nomeFich o nome do ficheiro de repartições, ex.: "fx_reparticoes.txt"
     * @param reps a classe RegistoReparticao que contém a lista onde vão ser guardadas as repartições
     */
    public static void lerReparticoes(String nomeFich, RegistoReparticao reps) {
        try (Scanner in = new Scanner(new File(nomeFich))) {
            String linha;
            String[] info;
            while (in.hasNext()) {
                linha = in.nextLine();
                if (linha.length() > 0) {
                    // exemplo de formato: Maia,1235,4470,A,B
                    info = linha.split(",");
                    String cidade = info[0];
                    int nrReparticao = Integer.parseInt(info[1]);
                    int codPostal = Integer.parseInt(info[2]);
                    Reparticao rep = new Reparticao(cidade, nrReparticao, codPostal);
                    for (int i = 3; i < info.length; i++) {
                        char codigo = info[i].toUpperCase().charAt(0);
                        Servico s = new Servico(codigo);
                        rep.getServicos().addServico(s);
                    }
                    reps.adicionarReparticao(rep);
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Ficheiro de repartições não encontrado!");
        } catch (NumberFormatException e) {
            System.out.println(MENSAGEM_ERRO_NUMEROS);
        }
    }

    /**
     * Lê um ficheiro de texto com as informações dos cidadãos e coloca em memória essas informações.
     *
     * @param nomeFich o nome do ficheiro de cidadãos, ex.: "fx_cidadaos.txt"
     * @param rp a classe RegistoReparticao que contém a lista onde são guardadas as repartições e os respectivos cidadãos
     */
    public static void lerCidadaos(String nomeFich, RegistoReparticao rp) {
        try (Scanner in = new Scanner(new File(nomeFich))) {

            String linha;
            String[] info;
            while (in.hasNext()) {
                linha = in.nextLine();
                if (linha.length() > 0) {
                    info = linha.split(",");
                    // exemplo de formato: Ana,111222333,ana@gmail.com,4200-072,1234
                    String nome = info[0];
                    String nrContribuinte = info[1];
                    String email = info[2];
                    CodigoPostal codPostal = new CodigoPostal(Integer.parseInt(info[3].split("-")[0]), info[3].split("-")[1]);
                    int reparticao = Integer.parseInt(info[4]);
                    Cidadao cidadao = new Cidadao(nrContribuinte, nome, email, codPostal, reparticao);
                    boolean adicionado = rp.adicionarCidadao(cidadao);
                    if (!adicionado) {
                        System.out.println("O cidadão com o NIF " + cidadao.getNrContribuinte() + " não foi"
                                + "adicionado a qualquer repartição!");
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Ficheiro de cidadãos não encontrado!");
        } catch (NumberFormatException e) {
            System.out.println(MENSAGEM_ERRO_NUMEROS);
        }
    }

    /**
     * Lê um ficheiro de texto com as informações das senhas e coloca em memória essas informações.
     *
     * @param nomeFich o nome do ficheiro de senhas, ex.: "fx_senhas.txt"
     * @param rp o registo de repartições
     */
    public static void lerSenhas(String nomeFich, RegistoReparticao rp) {
        try (Scanner in = new Scanner(new File(nomeFich))) {

            String linha;
            String[] info;
            while (in.hasNext()) {
                linha = in.nextLine();
                if (linha.length() > 0) {
                    info = linha.split(",");
                    // exemplo de formato: 111222333,A,1
                    String nrContribuinte = info[0];
                    char codigoAssunto = info[1].toUpperCase().charAt(0);

                    Reparticao r = rp.procurarReparticao(nrContribuinte);
                    if (r != null) {
                        Cidadao c = r.getListaCidadao().cidadaoExiste(nrContribuinte);
                        DoublyLinkedList<Cidadao> filaDoCidadao = r.getServicos().obterFilaPorCodigoDeServico(codigoAssunto);
                        List<DoublyLinkedList<Cidadao>> outrasFilas = r.getServicos().obterOutrasFilas(codigoAssunto);
                        if (filaDoCidadao != null) {
                            adicionarCidadaoAFila(c, filaDoCidadao, outrasFilas);
                        }
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Ficheiro de senhas não encontrado!");
        } catch (NumberFormatException e) {
            System.out.println(MENSAGEM_ERRO_NUMEROS);
        }
    }

    /**
     * Adiciona um cidadão à fila por ele pretendida e garante que não é atendido ao mesmo tempo noutras filas.
     * @param c o cidadão a ser adicionado
     * @param fila a fila a que vai ser adicionado
     * @param filas todas as filas dos serviços da repartição 
     */
    private static void adicionarCidadaoAFila(Cidadao c, DoublyLinkedList<Cidadao> fila, List<DoublyLinkedList<Cidadao>> filas) {
        int pos = obterPosicaoDisponivelValida(c, fila, filas);
        if (pos == -1) {
            while (!naoOcupaAMesmaPosicaoNoutrasFilas(c, filas, fila.size())) {
                fila.addLast(null);
            }
            fila.addLast(c);

        } else {
            ListIterator<Cidadao> it = fila.listIterator();
            for (int i = 0; i <= pos; i++) {
                it.next();
            }
            it.set(c);
        }

    }
    
    /**
     * Retorna a posição disponível e válida da fila a que o cidadão vai ser adicionado.
     * @param c o cidadão a ser adicionado à fila
     * @param fila a fila a que vai ser adicionado
     * @param filas a lista de todas as filas dos serviços da repartição
     * @return a posição disponível e válida da fila a que pretende ser adicionado, -1 se não existir posição disponível e válida.
     */
    private static int obterPosicaoDisponivelValida(Cidadao c, DoublyLinkedList<Cidadao> fila, List<DoublyLinkedList<Cidadao>> filas) {
        ListIterator<Cidadao> it = fila.listIterator();
        // percorrer a fila
        for (int i = 0; i < fila.size(); i++) {
            Cidadao cid = it.next();
            // se o lugar estiver disponível
            if (cid == null) {
                // verificar se o cidadão não ocupa a mesma posição nas outras filas
                if (naoOcupaAMesmaPosicaoNoutrasFilas(c, filas, i)) {
                    return i;
                }
            }
        }
        return -1;
    }
    
    /**
     * Método para verificar que o cidadão não ocupa a mesma posição noutras filas para além da que vai ser adicionado.
     * @param c o cidadão a ser adicionado à fila
     * @param filas as filas dos serviços da repartição
     * @param posicao a posição do cidadão
     * @return true se o cidadão não ocupa a mesma posição em nenhuma outra fila, false caso contrário
     */
    private static boolean naoOcupaAMesmaPosicaoNoutrasFilas(Cidadao c, List<DoublyLinkedList<Cidadao>> filas, int posicao) {

        ListIterator<Cidadao> it;
        // percorrer as filas
        for (DoublyLinkedList<Cidadao> fila : filas) {
            //  iterador para cada fila
            it = fila.listIterator();
            //  só faz sentido percorrer a fila se a posição existir
            if (fila.size() > posicao) {
                // avançar o iterador até à posição pretendida menos uma
                for (int i = 0; i < posicao; i++) {
                    it.next();
                }
                Cidadao cid = (Cidadao) it.next();
                if (cid != null) {
                    if (cid.equals(c)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
