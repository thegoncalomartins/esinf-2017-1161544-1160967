/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 * Classe hora, usada para representar objetos do tipo hora.
 *
 * @author Gonçalo Martins 1161544
 * @author João Dias 1160967
 */
public class Hora {
    
    /**
     * A hora.
     */
    private int hora;
    
    /**
     * Os minutos.
     */
    private int minutos;
    
    /**
     * Cria uma nova instância do tipo hora.
     * @param hora a hora
     * @param minutos os minutos
     */
    public Hora(int hora, int minutos) {
        setHora(hora);
        setMinutos(minutos);
    }
    
    /**
     * 
     * @return a hora
     */
    public int getHora() {
        return hora;
    }
    
    /**
     * 
     * @return os minutos 
     */
    public int getMinutos() {
        return minutos;
    }

    /**
     * 
     * @param hora 
     */
    private void setHora(int hora) {
        if (hora < 0 || hora > 23 ) {
            throw new IllegalArgumentException("Hora inválida.");
        }
        this.hora = hora;
    }

    /**
     * 
     * @param minutos 
     */
    private void setMinutos(int minutos) {
        if (minutos < 0 || minutos > 59 ) {
            throw new IllegalArgumentException("Minutos inválidos.");
        }
        this.minutos = minutos;
    }
    
    @Override
    public String toString() {
        return String.format("%d:%dh%n", hora, minutos);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        
        Hora outra = (Hora) obj;
        
        return this.hora == outra.hora && this.minutos == outra.minutos;
    }

    @Override
    public int hashCode() {
        return this.hora + this.minutos;
    }

    /**
     * Método que verifica se esta hora é mais recente ou igual à passada por parâmetro.
     * @param outraHora a hora a ser comparada
     * @return true se esta hora for maior que a recebida por parâmetro.
     */
    public boolean isMaiorOuIgual(Hora outraHora) {
        if (this.hora > outraHora.hora) {
            return true;
        }
        
        if (this.hora == outraHora.hora) {
            if (this.minutos >= outraHora.minutos) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Método que subtrai 10 minutos à hora atual.
     */
    public void subtrair10Minutos() {
        if (minutos >= 10) {
            minutos -= 10;
        } else {
            hora--;
            minutos += 50;
        }
    }
    
}
