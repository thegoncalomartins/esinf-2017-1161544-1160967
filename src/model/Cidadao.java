/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.apache.commons.validator.EmailValidator;

/**
 * Classe cidadão, usada para representar objetos do tipo cidadão.
 *
 * @author Gonçalo Martins 1161544
 * @author João Dias 1160967
 */
public class Cidadao {

    /**
     * O número de contribuinte do cidadão. Ex.: 190245987
     */
    private String nrContribuinte;

    /**
     * O nome do cidadão.
     */
    private String nome;

    /**
     * O endereço de correio eletrónico do cidadão. Ex.: nome@yahoo.com
     */
    private String email;

    /**
     * O código postal da residência do cidadão.
     */
    private CodigoPostal codPostal;
    
    /**
     * O número da repartição a que pertence o cidadão.
     */
    private int nrReparticao;

    /**
     * Cria um novo cidadão com as informações passadas por parâmetro.
     *
     * @param nif o número de contribuinte do cidadão
     * @param nome o nome do cidadão
     * @param email o email do cidadão
     * @param codPostal o código postal da residência do cidadão
     * @param nrReparticao o número da repartição a que pertence o cidadão
     */
    public Cidadao(String nif, String nome, String email, CodigoPostal codPostal, int nrReparticao) {
        setNrContribuinte(nif);
        this.nome = nome;
        setEmail(email);
        setCodPostal(codPostal);
        this.nrReparticao = nrReparticao;
    }

    /**
     * @return o NIF do cidadão
     */
    public String getNrContribuinte() {
        return nrContribuinte;
    }

    /**
     * @return o nome do cidadão
     */
    public String getNome() {
        return nome;
    }

    /**
     * @return o e-mail do cidadão
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return o código postal do cidadão
     */
    public CodigoPostal getCodPostal() {
        return codPostal;
    }

    /**
     * @param nrContribuinte o NIF a definir
     */
    public void setNrContribuinte(String nrContribuinte) {
        if (isNumeric(nrContribuinte)) {
            this.nrContribuinte = nrContribuinte;
        } else {
            throw new IllegalArgumentException("Número de contribuinte inválido!");
        }
    }

    /**
     * @param nome o nome a definir
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @param email o e-mail a definir
     */
    public void setEmail(String email) {
        EmailValidator e = EmailValidator.getInstance();
        if (e.isValid(email)) {
            this.email = email;
        } else {
            throw new IllegalArgumentException("E-mail inválido!");
        }

    }

    /**
     * @param codPostal o código postal a definir
     */
    public void setCodPostal(CodigoPostal codPostal) {
        this.codPostal = codPostal;
    }
    
    /**
     * @return o número da repartição a que pertence o cidadão
     */
    public int getNrReparticao() {
        return nrReparticao;
    }

    /**
     * @param nrReparticao o novo número da repartição do cidadão
     */
    public void setNrReparticao(int nrReparticao) {
        this.nrReparticao = nrReparticao;
    }

    @Override
    public String toString() {
        return String.format("Nome: %s%nNrº Contribuinte: %s%nEmail: %s%nCódigo Postal: %s%nNº Repartição: %d%n",
                nome, nrContribuinte, email, codPostal, nrReparticao);
    }

    /**
     * Verifica se uma dada string contém apenas caracteres numéricos.
     *
     * @param s a string a verificar
     * @return true se só contiver caracteres numéricos, false caso contrário.
     */
    private static boolean isNumeric(String s) {
        final int limInf = 48; // código ascii do '0'
        final int limSup = 57; // código ascii do '9'
        int asciiCode;
        for (int i = 0; i < s.length(); i++) {
            asciiCode = (int) s.charAt(i);
            if (!(asciiCode >= limInf && asciiCode <= limSup)) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Cidadao)) {
            return false;
        }

        Cidadao that = (Cidadao) obj;

        return this.nrContribuinte.equalsIgnoreCase(that.nrContribuinte);
    }

    @Override
    public int hashCode() {
        return this.nrContribuinte.hashCode();
    }

    /**
     * Classe código postal, usada para representar objetos do tipo Código Postal.
     */
    public static class CodigoPostal {

        /**
         * A região do código postal, ex.: 4470
         */
        private int regiao;

        /**
         * A sub-região do código postal, ex.: 090
         */
        private String subRegiao;

        /**
         * Cria um novo código postal com todos os atributos.
         *
         * @param regiao a região do local
         * @param subRegiao a sub-região do local
         */
        public CodigoPostal(int regiao, String subRegiao) {
            setRegiao(regiao);
            setSubRegiao(subRegiao);
        }

        /**
         * @return a região do código postal
         */
        public int getRegiao() {
            return regiao;
        }

        /**
         * @return a sub-região do código postal
         */
        public String getSubRegiao() {
            return subRegiao;
        }

        /**
         * @param regiao a região a definir
         */
        public void setRegiao(int regiao) {
            final int limInfRegiao = 1000;
            final int limSupRegiao = 9999;

            if (!(regiao >= limInfRegiao && regiao <= limSupRegiao)) {
                throw new IllegalArgumentException("Região de código postal inválida!");
            }
            this.regiao = regiao;

        }

        /**
         * @param subRegiao a sub-região a definir
         */
        public void setSubRegiao(String subRegiao) {
            final int nrDigitosSubRegiao = 3;
            if (isNumeric(subRegiao) && subRegiao.length() == nrDigitosSubRegiao) {
                this.subRegiao = subRegiao;
            } else {
                throw new IllegalArgumentException("Sub-região de código postal inválida!");
            }
        }

        @Override
        public String toString() {
            return String.format("%d-%s", regiao, subRegiao);
        }
    }

    

}
