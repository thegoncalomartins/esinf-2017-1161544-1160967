/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashSet;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import utils.DoublyLinkedList;
import utils.Hora;

/**
 * Classe RegistoReparticao.
 *
 * @author Gonçalo Martins 1161544
 * @author João Dias 1160967
 */
public class RegistoReparticao {

    /**
     * A lista de repartições.
     */
    private DoublyLinkedList<Reparticao> reparticoes;

    /**
     * A hora de abertura das repartições.
     */
    private static final int HORA_ABERTURA = 9;

    /**
     * Os minutos da hora de abertura das repartições.
     */
    private static final int MIN_ABERTURA = 0;

    /**
     * A hora de fecho das repartições.
     */
    private static final int HORA_FECHO = 15;

    /**
     * Os minutos de fecho das repartições.
     */
    private static final int MIN_FECHO = 30;

    /**
     * Construtor.
     */
    public RegistoReparticao() {
        reparticoes = new DoublyLinkedList<>();
    }

    /**
     * Retorna a lista de reparticoes;
     *
     * @return a lista de reparticoes
     */
    public DoublyLinkedList<Reparticao> getReparticoes() {
        return reparticoes;
    }

    /**
     * Procura e retorna a repartição recebida, se existir. Retorna null se a repartição não existir na lista.
     *
     * @param r a repartição a procurar
     * @return a repartição com o número passado por parâmetro ou null caso não exista na lista
     */
    public Reparticao procurarReparticao(Reparticao r) {
        ListIterator<Reparticao> it = getReparticoes().listIterator();
        while (it.hasNext()) {
            if (r.equals(it.next())) {
                return r;
            }
        }
        return null;

    }

    /**
     * Procura uma repartição recebendo por parâmetro um cidadão pertencente a esta.
     *
     * @param nrContribuinteCidadao
     * @return a repartição a que pertence o cidadão, null caso este não pertença a nenhuma repartição.
     */
    public Reparticao procurarReparticao(String nrContribuinteCidadao) {
        ListIterator<Reparticao> it = getReparticoes().listIterator();
        Reparticao r;
        while (it.hasNext()) {
            r = it.next();
            if (r.getListaCidadao().cidadaoExiste(nrContribuinteCidadao) != null) {
                return r;
            }
        }
        return null;
    }

    /**
     * Remove e devolve os cidadãos cujo código postal é igual ao passado por parâmetro.
     *
     * @param codigoPostal o código postal
     * @return os cidadãos removidos
     */
    private Set<Cidadao> removerCidadaosPorCodigoPostal(int codigoPostal) {
        Set<Cidadao> cidadaosRemovidos = new HashSet<>();

        ListIterator<Reparticao> it = reparticoes.listIterator();
        Reparticao r;
        while (it.hasNext()) {
            r = it.next();
            cidadaosRemovidos.addAll(r.getListaCidadao().removerCidadaosPorCodigoPostal(codigoPostal));
        }

        return cidadaosRemovidos;
    }

    /**
     * Muda o nº de repartição dos cidadãos recebidos por parâmetro para o nº de repartição recebido por parâmetro.
     *
     * @param cidadaos a lista de cidadãos
     * @param nrReparticao o novo nº de repartição
     */
    private void atualizarNrReparticaoDosCidadaos(Set<Cidadao> cidadaos, int nrReparticao) {
        for (Cidadao c : cidadaos) {
            c.setNrReparticao(nrReparticao);
        }
    }

    /**
     * Adiciona uma repartição à lista de repartições se a repartição não existir na lista. Remove de outras repartições os cidadãos que tenham o código postal igual à nova reparticão e adiciona-os a ela. Retorna true se a repartição for adicionada, senão retorna false.
     *
     * @param r
     * @return true se a repartição for adicionada, senão retorna false
     */
    public boolean adicionarReparticao(Reparticao r) {
        if (procurarReparticao(r) != null) {
            return false;
        }
        Set<Cidadao> cidadaosRemovidos = removerCidadaosPorCodigoPostal(r.getCodigoPostal());
        atualizarNrReparticaoDosCidadaos(cidadaosRemovidos, r.getNrReparticao());
        r.getListaCidadao().getCidadaos().addAll(cidadaosRemovidos);
        getReparticoes().addLast(r);
        return true;
    }

    /**
     * Devolve a repartição com o código postal mais próximo do código postal passado por parâmetro. Devolve null se não houver repartições na lista.
     *
     * @param codigoPostal o código postal
     * @return a repartição com o código postal mais próximo ou null caso a lista esteja vazia
     */
    private Reparticao encontrarReparticaoComCodigoPostalMaisProximo(int codigoPostal, DoublyLinkedList<Reparticao> dll) {
        ListIterator<Reparticao> it = dll.listIterator();
        if (!it.hasNext()) {
            return null;
        }
        int diferencaMin = 8999;       // 8999 é a diferença máxima entre códigos postais
        int diferenca;
        Reparticao resultado = null;
        Reparticao r;
        while (it.hasNext()) {
            r = it.next();
            diferenca = Math.abs(codigoPostal - r.getCodigoPostal());
            if (diferenca <= diferencaMin) {
                diferencaMin = diferenca;
                resultado = r;
            }
        }
        return resultado;
    }

    /**
     * Remove a repartição com o número recebido por parâmetro. Passa os cidadãos da repartição para a repartição com o código postal mais pŕoximo.
     *
     * @param nrReparticao o número da repartição a remover
     * @return true se a repartição for removida, false caso contrário
     */
    public boolean removerReparticao(int nrReparticao) {
        ListIterator<Reparticao> it = getReparticoes().listIterator();
        Reparticao r;

        while (it.hasNext()) {
            r = it.next();
            if (r.getNrReparticao() == nrReparticao) {
                Set<Cidadao> cidadaosAMover = r.getListaCidadao().getCidadaos();
                int codigoPostal = r.getCodigoPostal();
                it.remove();
                Reparticao nova = encontrarReparticaoComCodigoPostalMaisProximo(codigoPostal, getReparticoes());
                if (nova != null) {
                    atualizarNrReparticaoDosCidadaos(cidadaosAMover, nova.getNrReparticao());
                    nova.getListaCidadao().getCidadaos().addAll(cidadaosAMover);
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Verifica se o cidadão passado por parâmetro existe nas repartições. Retorna true se existir, false caso contrário.
     *
     * @param cidadaoProcurado o cidadão
     * @return true se o cidadão existir, false caso contrário
     */
    private boolean cidadaoExiste(Cidadao c) {
        ListIterator<Reparticao> it = getReparticoes().listIterator();
        Reparticao r;

        while (it.hasNext()) {
            r = it.next();
            if (r.getListaCidadao().cidadaoExiste(c)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Encontra todas as repartições com o número passado por parâmetro.
     *
     * @param nrReparticao o número das repartições
     * @return a lista de repartições com o mesmo número passado por parâmetro
     */
    private DoublyLinkedList<Reparticao> encontrarReparticoesComOMesmoNumero(int nrReparticao) {
        DoublyLinkedList<Reparticao> reps = new DoublyLinkedList<>();

        for (Reparticao r : getReparticoes()) {
            if (r.getNrReparticao() == nrReparticao) {
                reps.addLast(r);
            }
        }

        return reps;
    }

    /**
     * Adiciona um cidadão à respectiva repartição caso o cidadão não exista e a repartição exista. Retorna true se for adicionado, false caso contrário.
     *
     * @param c o cidadão a ser adicionado
     * @return true se o cidadão for adicionado, false caso contrário
     */
    public boolean adicionarCidadao(Cidadao c) {
        if (cidadaoExiste(c)) {
            return false;
        }

        DoublyLinkedList<Reparticao> reps = encontrarReparticoesComOMesmoNumero(c.getNrReparticao());

        Reparticao r;
        if (reps.isEmpty()) {
            return false;
        } else {
            r = encontrarReparticaoComCodigoPostalMaisProximo(c.getCodPostal().getRegiao(), reps);
            c.setNrReparticao(r.getNrReparticao());
        }

        r.getListaCidadao().getCidadaos().add(c);
        return true;
    }

    /**
     * Escreve as informações de cada repartição (Número e cidade) e respetivos cidadãos.
     */
    public void cidadaosAfetosACadaReparticao() {
        ListIterator<Reparticao> it = reparticoes.listIterator();
        Reparticao r;
        ListaCidadao lc;
        while (it.hasNext()) {
            r = it.next();
            System.out.println("Repartição: " + r.getNrReparticao() + " " + r.getCidade() + "\n");
            lc = r.getListaCidadao();
            for (Cidadao c : lc.getCidadaos()) {
                System.out.println(c.getNrContribuinte());
            }
        }
    }

    /**
     * Cria um mapa com os serviços e respetiva contagem de cidadãos nas filas (em todas as repartições).
     *
     * @return o respetivo mapa
     */
    public Map<Servico, Integer> servicosComMaiorProcura() {
        Map<Servico, Integer> servicos = new TreeMap<>();

        ListIterator<Reparticao> it = reparticoes.listIterator();
        Reparticao r;
        int count;
        while (it.hasNext()) {
            r = it.next();
            for (Servico s : r.getServicos().getServicos()) {
                count = s.contagemDeCidadaosNaFila();
                if (servicos.containsKey(s)) {
                    Integer conta = servicos.get(s);
                    servicos.replace(s, conta + count);
                } else {
                    servicos.put(s, count);
                }
            }
        }

        return servicos;
    }

    /**
     * Método que permite saber os cidadãos já atendidos em cada serviço a uma dada hora.
     *
     * @param r a repartição a ser conhecida a sua utilização
     * @param hora a hora máxima a que já foram atendidos cidadãos.
     * @return um mapa com os serviços e as filas dos cidadãos já atendidos àquela hora, null se a hora ficar fora do intervalo de funcionamento da repartição.
     */
    public Map<Servico, DoublyLinkedList<Cidadao>> cidadaosAtendidos(Reparticao r, Hora hora) {

        if (hora.isMaiorOuIgual(new Hora(HORA_ABERTURA, MIN_ABERTURA))
                && new Hora(HORA_FECHO, MIN_FECHO).isMaiorOuIgual(hora)) {
            Map<Servico, DoublyLinkedList<Cidadao>> cidadaosAtendidos = new TreeMap<>();
            int nCidadaosAtendidos = numeroCidadaosAVerificar(hora);
            ListIterator<Cidadao> it;
            for (Servico s : r.getServicos().getServicos()) {
                int count = 0;
                it = s.getFila().listIterator();
                DoublyLinkedList<Cidadao> cids = new DoublyLinkedList<>();

                while (it.hasNext() && count < nCidadaosAtendidos) {

                    Cidadao c = it.next();
                    if (c != null) {
                        cids.addLast(c);
                    }

                    count++;
                }

                cidadaosAtendidos.put(s, cids);

            }

            return cidadaosAtendidos;
        }

        return null;
    }

    /**
     * Retorna o número de cidadãos atendidos (em média).
     *
     * @param tempo a hora máxima a que já foram atendidos cidadãos.
     * @return o número de cidadãos já atendidos
     */
    public int numeroCidadaosAVerificar(Hora tempo) {
        int count = -1;
        // criar uma cópia, para não alterar a instância atual
        Hora nova = new Hora(tempo.getHora(), tempo.getMinutos());

        do {
            nova.subtrair10Minutos();
            count++;
        } while (nova.isMaiorOuIgual(new Hora(HORA_ABERTURA, MIN_ABERTURA)));

        return count;
    }
}
