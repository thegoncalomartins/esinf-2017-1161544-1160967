package model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Classe ListaCidadao.
 *
 * @author Gonçalo Martins 1161544
 * @author João Dias 1160967
 */
public class ListaCidadao {

    /**
     * Lista de cidadãos e respetivas senhas.
     */
    private Set<Cidadao> cidadaos;

    /**
     * Construtor.
     */
    public ListaCidadao() {
        cidadaos = new HashSet<>();
    }

    /**
     * Devolve a lista de cidadãos.
     *
     * @return a lista de cidadãos
     */
    public Set<Cidadao> getCidadaos() {
        return cidadaos;
    }
    
    

    /**
     * Remove e devolve os cidadãos cujo código postal é igual ao passado por parâmetro.
     *
     * @param codigoPostal o código postal
     * @return os cidadãos removidos
     */
    public Set<Cidadao> removerCidadaosPorCodigoPostal(int codigoPostal) {
        Set<Cidadao> cidadaosRemovidos = new HashSet<>();

        Iterator it = getCidadaos().iterator();
        Cidadao c;
        while(it.hasNext()) {
            c = (Cidadao) it.next();
            if (codigoPostal ==  c.getCodPostal().getRegiao()) {
                cidadaosRemovidos.add(c);
                it.remove();
            }
        }
        return cidadaosRemovidos;

    }

    /**
     * Verifica se o cidadão passado por parâmetro existe na lista de cidadãos. Retorna true se existir, false caso contrário.
     *
     * @param cidadaoProcurado o cidadão
     * @return true se o cidadão existir na lista, false caso contrário
     */
    public boolean cidadaoExiste(Cidadao cidadaoProcurado) {
        for (Cidadao c : getCidadaos()) {
            if (cidadaoProcurado.equals(c)) {
                return true;
            }
        }
        return false;
    }
    
    public Cidadao cidadaoExiste(String nrContribuinteCidadao) {
        for (Cidadao c : getCidadaos()) {
            if (nrContribuinteCidadao.equalsIgnoreCase(c.getNrContribuinte())) {
                return c;
            }
        }
        return null;
    }
    
//    // alínea h)
//    /**
//     * Substitui todas as senhas tiradas por um cidadão, por um set de senhas vazio.
//     * @param c cidadão a abandonar as filas para o qual tirou senha(s). 
//     */
//    public void abandonarFila(Cidadao c) {
//        cidadaos.replace(c, new TreeSet<>());
//    }
}
