/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import utils.DoublyLinkedList;

/**
 * Classe ListaServicos.
 *
 * @author Gonçalo Martins 1161544
 * @author João Dias 1160967
 */
public class ListaServicos {
    
    /**
     * A lista de serviços da repartição;.
     */
    private Set<Servico> servicos;
    
    /**
     * Cria uma nova lista de serviços
     */
    public ListaServicos() {
        servicos = new TreeSet<>();
    }

    /**
     * @return a lista de servicos
     */
    public Set<Servico> getServicos() {
        return servicos;
    }
    
    /**
     * Adiciona um serviço à lista de serviços, se ele ainda não existir.
     * 
     * @param servicoAAdicionar o serviço a adicionar
     * @return true se adicionou o serviço, false caso contrário.
     */
    public boolean addServico(Servico servicoAAdicionar) {
        for (Servico s : servicos) {
            if (servicoAAdicionar == s) {
                return false;
            }
        }
        return servicos.add(servicoAAdicionar);
    }
    
    /**
     * Devolve a fila correspondente ao serviço com o código igual ao código passado por parâmetro.
     * 
     * @param codigo o código do serviço
     * @return a fila correspondente ao serviço com o código igual ao código passado por parâmetro, null se não encontrar o serviço
     */
    public DoublyLinkedList<Cidadao> obterFilaPorCodigoDeServico(char codigo) {
        for (Servico s : getServicos()) {
            if (s.getCodigo() == codigo) {
                return s.getFila();
            }
        }
        return null;
    }
    
    /**
     * Devolve as filas correspondente aos serviços com códigos diferentes do passado por parâmetro.
     * 
     * @param codigo o código do serviço
     * @return as filas correspondentes aos serviços com códigos diferentes do passado por parâmetro.
     */
    public List<DoublyLinkedList<Cidadao>> obterOutrasFilas(char codigo) {
        List<DoublyLinkedList<Cidadao>> filas = new ArrayList<>();
        for (Servico s : getServicos()) {
            if (s.getCodigo() != codigo) {
                filas.add(s.getFila());
            }
        }
        return filas;
    }
    
}
