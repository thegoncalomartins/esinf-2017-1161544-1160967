/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * Classe repartiçao.
 *
 * @author Gonçalo Martins 1161544
 * @author João Dias 1160967
 */
public class Reparticao {

    /**
     * A cidade onde se encontra a repartiçao.
     */
    private String cidade;

    /**
     * O numero da repartiçao.
     */
    private int nrReparticao;

    /**
     * O codigo postal da reparticao.
     */
    private int codigoPostal;

    /**
     * A lista de serviços da repartição;.
     */
    private ListaServicos servicos;

    /**
     * ListaCidadao.
     */
    private ListaCidadao listaCidadao;

    /**
     * Cria uma nova repartiçao com as informações passadas por parâmetro.
     *
     * @param cidade a cidade da repartiçao
     * @param nrReparticao o numero da reparticao
     * @param codigoPostal o codigo postal da reparticao
     */
    public Reparticao(String cidade, int nrReparticao, int codigoPostal) {
        this.cidade = cidade;
        this.nrReparticao = nrReparticao;
        setCodigoPostal(codigoPostal);
        servicos = new ListaServicos();
        listaCidadao = new ListaCidadao();
    }

    /**
     * @return a cidade
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * @param cidade a nova cidade
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    /**
     * @return o numero da reparticao
     */
    public int getNrReparticao() {
        return nrReparticao;
    }

    /**
     * @param nrReparticao o novo numero da repartiçao
     */
    public void setNrReparticao(int nrReparticao) {
        this.nrReparticao = nrReparticao;
    }

    /**
     * @return o codigo postal da repartiçao
     */
    public int getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * @param codigoPostal o novo codigo postal
     */
    public void setCodigoPostal(int codigoPostal) {
        final int limInfRegiao = 1000;
        final int limSupRegiao = 9999;

        if (!(codigoPostal >= limInfRegiao && codigoPostal <= limSupRegiao)) {
            throw new IllegalArgumentException("Região de código postal inválida!");
        }
        this.codigoPostal = codigoPostal;
    }

    /**
     * @return a lista de servicos
     */
    public ListaServicos getServicos() {
        return servicos;
    }

    /**
     * Devolve listaCidadao.
     *
     * @return listaCidadao
     */
    public ListaCidadao getListaCidadao() {
        return listaCidadao;
    }

    @Override
    public String toString() {
        return String.format("Cidade: %s%nNrº Reparticão: %d%nCódigo Postal: %d%nServiços: %s%n",
                cidade, nrReparticao, codigoPostal, servicos);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Reparticao that = (Reparticao) obj;

        return this.nrReparticao == that.nrReparticao && this.codigoPostal == that.codigoPostal;
    }

    @Override
    public int hashCode() {
        return this.codigoPostal + this.nrReparticao;
    }

    /**
     * Permite ao cidadão abandonar todas as filas para que tirou senha.
     *
     * @param c o cidadão
     */
    public void abandonarFilas(Cidadao c) {
        for (Servico s : getServicos().getServicos()) {
            s.removerCidadaoDaFila(c);
        }
    }

}
