/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ListIterator;
import utils.DoublyLinkedList;

/**
 * Classe Servico.
 *
 * @author Gonçalo Martins 1161544
 * @author João Dias 1160967
 */
public class Servico implements Comparable {

    /**
     * O código do serviço.
     */
    private char codigo;

    /**
     * A fila de cidadãos do serviço.
     */
    private DoublyLinkedList<Cidadao> fila;

    /**
     * Construtor.
     *
     * @param codigo o código do serviço
     */
    public Servico(char codigo) {
        this.codigo = codigo;
        fila = new DoublyLinkedList<>();
    }

    /**
     * Devolve o código do serviço.
     *
     * @return o código do serviço
     */
    public char getCodigo() {
        return codigo;
    }

    /**
     * Devolve a fila de cidadãos do serviço.
     *
     * @return a fila de cidadãos do serviço
     */
    public DoublyLinkedList<Cidadao> getFila() {
        return fila;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Servico that = (Servico) obj;

        return this.codigo == that.codigo;
    }

    @Override
    public int hashCode() {
        return this.codigo;
    }

    @Override
    public int compareTo(Object o) {
        if (this.codigo < ((Servico) o).codigo) {
            return -1;
        } else if (this.codigo > ((Servico) o).codigo) {
            return 1;
        }
        return 0;
    }

    /**
     * Conta o número de cidadãos presentes na fila deste serviço.
     * @return a contagem de cidadãos
     */
    public int contagemDeCidadaosNaFila() {
        int count = 0;
        for (Cidadao c : getFila()) {
            if (c != null) {
                count++;
            }
        }

        return count;
    }
    
    /**
     * Remove o cidadão recebido por parâmetro da fila.
     * 
     * @param c o cidadão
     */
    public void removerCidadaoDaFila(Cidadao c) {
        ListIterator<Cidadao> it = getFila().listIterator();
        while (it.hasNext()) {
            if (it.next().equals(c)) {
                it.set(null);
            }
        }
    }

}
