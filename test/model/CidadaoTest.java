/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import model.Cidadao.CodigoPostal;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Classe teste para a classe cidadão.
 *
 * @author Gonçalo Martins 1161544
 * @author João Dias 1160967
 */
public class CidadaoTest {
    
    private static final String NR_CONTRIBUINTE = "999999999";
    private static final String NOME = "NOME";
    private static final String EMAIL = "email@email.com";
    private static final CodigoPostal COD_POSTAL = new CodigoPostal(4475, "217");
    private static final int NR_REPARTICAO = 1234;
    
    @Test
    public void ensureSameObjectIsEqual() {
        
        Cidadao expected = new Cidadao(NR_CONTRIBUINTE, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO);
        
        assertEquals(expected, expected);
    }
    
    @Test
    public void ensureDifferentObjectAreNotEqual() {

        Cidadao expected = new Cidadao(NR_CONTRIBUINTE, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO);
        
        Object result = new Object();
        
        assertNotEquals(expected, result);
    }
    
    @Test
    public void ensureSameContribuinteObjectsAreEqual() {

        Cidadao expected = new Cidadao(NR_CONTRIBUINTE, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO);
        
        Cidadao result = new Cidadao(NR_CONTRIBUINTE, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO);
        
        assertEquals(expected, result);
    }
    
    @Test
    public void ensureDifferentContribuinteObjectsAreNotEqual() {
 
        Cidadao expected = new Cidadao(NR_CONTRIBUINTE, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO);
        
        String nrContribuinte2 = "111111111";
        Cidadao result = new Cidadao(nrContribuinte2, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO);
        
        assertNotEquals(expected, result);
    }
    
    @Test
    public void ensureHashCodeIsCorrect() {

        Cidadao c = new Cidadao(NR_CONTRIBUINTE, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO);
        
        int expected = NR_CONTRIBUINTE.hashCode();
        int result = c.hashCode();
        
        assertEquals(expected, result);
    }
    
    
    
}
