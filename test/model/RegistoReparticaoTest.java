/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.DoublyLinkedList;
import utils.Hora;
import utils.LeituraFicheiros;

/**
 * Classe teste para RegistoReparticao.
 *
 * @author Gonçalo Martins 1161544
 * @author João Dias 1160967
 */
public class RegistoReparticaoTest {

    private static final String CIDADE_1 = "CIDADE1";
    private static final String CIDADE_2 = "CIDADE2";
    private static final String CIDADE_3 = "CIDADE3";
    private static final int NR_REPARTICAO_1 = 1111;
    private static final int NR_REPARTICAO_2 = 2222;
    private static final int NR_REPARTICAO_3 = 3333;
    private static final int CODIGO_POSTAL_1 = 1000;
    private static final int CODIGO_POSTAL_2 = 2000;
    private static final int CODIGO_POSTAL_3 = 3000;

    private static final String NR_CONTRIBUINTE = "999999999";
    private static final String NOME = "NOME";
    private static final String EMAIL = "email@email.com";
    private static final Cidadao.CodigoPostal COD_POSTAL = new Cidadao.CodigoPostal(2000, "217");
    private static final int NR_REPARTICAO_CIDADAO = 1111;

    @Test
    public void ensureProcurarReparticaoReturnsTheReparticao() {
        RegistoReparticao rp = new RegistoReparticao();

        Reparticao r1 = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        Reparticao r2 = new Reparticao(CIDADE_2, NR_REPARTICAO_2, CODIGO_POSTAL_2);

        rp.getReparticoes().addLast(r1);
        rp.getReparticoes().addLast(r2);

        Reparticao expected = r1;
        Reparticao result = rp.procurarReparticao(r1);

        assertEquals(expected, result);
    }

    @Test
    public void ensureProcurarReparticaoReturnsNull() {
        RegistoReparticao rp = new RegistoReparticao();

        Reparticao r1 = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        Reparticao r2 = new Reparticao(CIDADE_2, NR_REPARTICAO_2, CODIGO_POSTAL_2);

        rp.getReparticoes().addLast(r1);

        Reparticao expected = null;
        Reparticao result = rp.procurarReparticao(r2);

        assertEquals(expected, result);
    }

    @Test
    public void ensureAdicionarReparticaoAddsReparticao() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);

        rp.adicionarReparticao(r);

        DoublyLinkedList<Reparticao> expected = new DoublyLinkedList<>();
        expected.addLast(r);
        DoublyLinkedList<Reparticao> result = rp.getReparticoes();
        assertEquals(expected, result);
    }

    @Test
    public void ensureAdicionarReparticaoAddedReparticaoReturnsTrue() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);

        boolean expected = true;
        boolean result = rp.adicionarReparticao(r);
        assertEquals(expected, result);
    }

    @Test
    public void ensureAdicionarReparticaoDoesntAddExistingReparticao() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        rp.getReparticoes().addLast(r);

        rp.adicionarReparticao(r);

        DoublyLinkedList<Reparticao> expected = new DoublyLinkedList<>();
        expected.addLast(r);
        DoublyLinkedList<Reparticao> result = rp.getReparticoes();
        assertEquals(expected, result);
    }

    @Test
    public void ensureAdicionarReparticaoExistingReparticaoReturnsFalse() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        rp.getReparticoes().addLast(r);

        boolean expected = false;
        boolean result = rp.adicionarReparticao(r);
        assertEquals(expected, result);
    }

    @Test
    public void ensureAdicionarReparticaoAddsCidadao() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r1 = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        Cidadao c = new Cidadao(NR_CONTRIBUINTE, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO_CIDADAO);
        r1.getListaCidadao().getCidadaos().add(c);
        rp.getReparticoes().addLast(r1);
        Reparticao r2 = new Reparticao(CIDADE_2, NR_REPARTICAO_2, CODIGO_POSTAL_2);

        rp.adicionarReparticao(r2);

        Set<Cidadao> expected = new HashSet<>();
        expected.add(c);
        Set<Cidadao> result = r2.getListaCidadao().getCidadaos();
        assertEquals(expected, result);
    }

    @Test
    public void ensureAdicionarReparticaoRemovesCidadao() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r1 = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        Cidadao c = new Cidadao(NR_CONTRIBUINTE, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO_CIDADAO);
        r1.getListaCidadao().getCidadaos().add(c);
        rp.getReparticoes().addLast(r1);
        Reparticao r2 = new Reparticao(CIDADE_2, NR_REPARTICAO_2, CODIGO_POSTAL_2);

        rp.adicionarReparticao(r2);

        Set<Cidadao> expected = new HashSet<>();
        Set<Cidadao> result = r1.getListaCidadao().getCidadaos();
        assertEquals(expected, result);
    }

    @Test
    public void ensureAdicionarReparticaoChangesCidadaoNrReparticao() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r1 = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        Cidadao c = new Cidadao(NR_CONTRIBUINTE, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO_CIDADAO);
        r1.getListaCidadao().getCidadaos().add(c);
        rp.getReparticoes().addLast(r1);
        Reparticao r2 = new Reparticao(CIDADE_2, NR_REPARTICAO_2, CODIGO_POSTAL_2);

        rp.adicionarReparticao(r2);

        int expected = NR_REPARTICAO_2;
        int result = c.getNrReparticao();
        assertEquals(expected, result);
    }

    @Test
    public void ensureRemoverReparticaoRemovesReparticao() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        rp.getReparticoes().addLast(r);

        rp.removerReparticao(r.getNrReparticao());

        DoublyLinkedList<Reparticao> expected = new DoublyLinkedList<>();
        DoublyLinkedList<Reparticao> result = rp.getReparticoes();

        assertEquals(expected, result);
    }

    @Test
    public void ensureRemoverReparticaoRemovedReparticaoReturnsTrue() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        rp.getReparticoes().addLast(r);

        boolean expected = true;
        boolean result = rp.removerReparticao(r.getNrReparticao());

        assertEquals(expected, result);
    }

    @Test
    public void ensureRemoverReparticaoNonExistingReparticaoDoesntChangeList() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        rp.getReparticoes().addLast(r);

        rp.removerReparticao(NR_REPARTICAO_2);

        DoublyLinkedList<Reparticao> expected = new DoublyLinkedList<>();
        expected.addLast(r);
        DoublyLinkedList<Reparticao> result = rp.getReparticoes();
        assertEquals(expected, result);
    }

    @Test
    public void ensureRemoverReparticaoNonExistingReparticaoReturnsFalse() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        rp.getReparticoes().addLast(r);

        boolean expected = false;
        boolean result = rp.removerReparticao(NR_REPARTICAO_2);

        assertEquals(expected, result);
    }

    @Test
    public void ensureRemoverReparticaoCidadaosAreMovedCorrectly() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r1 = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        Reparticao r2 = new Reparticao(CIDADE_2, NR_REPARTICAO_2, CODIGO_POSTAL_2);
        Reparticao r3 = new Reparticao(CIDADE_3, NR_REPARTICAO_3, CODIGO_POSTAL_3);
        Cidadao c = new Cidadao(NR_CONTRIBUINTE, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO_CIDADAO);
        r3.getListaCidadao().getCidadaos().add(c);
        rp.getReparticoes().addLast(r1);
        rp.getReparticoes().addLast(r2);
        rp.getReparticoes().addLast(r3);

        rp.removerReparticao(r3.getNrReparticao());

        Set<Cidadao> expected = new HashSet<>();
        expected.add(c);
        Set<Cidadao> result = r2.getListaCidadao().getCidadaos();
        assertEquals(expected, result);
    }

    @Test
    public void ensureRemoverReparticaoChangesCidadaosNrReparticao() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r1 = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        Reparticao r2 = new Reparticao(CIDADE_2, NR_REPARTICAO_2, CODIGO_POSTAL_2);
        Reparticao r3 = new Reparticao(CIDADE_3, NR_REPARTICAO_3, CODIGO_POSTAL_3);
        Cidadao c = new Cidadao(NR_CONTRIBUINTE, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO_CIDADAO);
        r3.getListaCidadao().getCidadaos().add(c);
        rp.getReparticoes().addLast(r1);
        rp.getReparticoes().addLast(r2);
        rp.getReparticoes().addLast(r3);

        rp.removerReparticao(r3.getNrReparticao());

        int expected = r2.getNrReparticao();
        int result = c.getNrReparticao();
        assertEquals(expected, result);
    }

    @Test
    public void ensureAdicionarCidadaoAddsCidadao() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        rp.getReparticoes().addLast(r);
        Cidadao c = new Cidadao(NR_CONTRIBUINTE, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO_1);

        rp.adicionarCidadao(c);

        Set<Cidadao> expected = new HashSet<>();
        expected.add(c);
        Set<Cidadao> result = r.getListaCidadao().getCidadaos();
        assertEquals(expected, result);
    }

    @Test
    public void ensureAdicionarCidadaoAddedCidadaoReturnsTrue() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        rp.getReparticoes().addLast(r);
        Cidadao c = new Cidadao(NR_CONTRIBUINTE, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO_1);

        boolean expected = true;
        boolean result = rp.adicionarCidadao(c);
        assertEquals(expected, result);
    }

    @Test
    public void ensureAdicionarCidadaoDoesntAddExistingCidadao() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        Cidadao c = new Cidadao(NR_CONTRIBUINTE, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO_1);
        r.getListaCidadao().getCidadaos().add(c);
        rp.getReparticoes().addLast(r);

        rp.adicionarCidadao(c);

        Set<Cidadao> expected = new HashSet<>();
        expected.add(c);
        Set<Cidadao> result = r.getListaCidadao().getCidadaos();
        assertEquals(expected, result);
    }

    @Test
    public void ensureAdicionarCidadaoExistingCidadaoReturnsFalse() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        Cidadao c = new Cidadao(NR_CONTRIBUINTE, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO_1);
        r.getListaCidadao().getCidadaos().add(c);
        rp.getReparticoes().addLast(r);

        boolean expected = false;
        boolean result = rp.adicionarCidadao(c);
        assertEquals(expected, result);
    }

    @Test
    public void ensureAdicionarCidadaoNonExistingReparticaoDoesntAddCidadao() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        rp.getReparticoes().addLast(r);
        Cidadao c = new Cidadao(NR_CONTRIBUINTE, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO_2);

        rp.adicionarCidadao(c);

        Set<Cidadao> expected = new HashSet<>();
        Set<Cidadao> result = r.getListaCidadao().getCidadaos();
        assertEquals(expected, result);
    }

    @Test
    public void ensureAdicionarCidadaoNonExistingReparticaoReturnsFalse() {
        RegistoReparticao rp = new RegistoReparticao();
        Reparticao r = new Reparticao(CIDADE_1, NR_REPARTICAO_1, CODIGO_POSTAL_1);
        rp.getReparticoes().addLast(r);
        Cidadao c = new Cidadao(NR_CONTRIBUINTE, NOME, EMAIL, COD_POSTAL, NR_REPARTICAO_2);

        boolean expected = false;
        boolean result = rp.adicionarCidadao(c);
        assertEquals(expected, result);
    }

    @Test
    public void ensureServicosComMaiorProcuraIsWorking() {
        RegistoReparticao rp = new RegistoReparticao();

        String cidade1 = "Porto";
        int nrReparticao1 = 1234;
        int codigoPostal1 = 4200;
        Reparticao r1 = new Reparticao(cidade1, nrReparticao1, codigoPostal1);
        Servico s1 = new Servico('A');
        Servico s2 = new Servico('B');
        r1.getServicos().getServicos().add(s1);
        r1.getServicos().getServicos().add(s2);

        rp.getReparticoes().addLast(r1);

        String cidade2 = "Maia";
        int nrReparticao2 = 1200;
        int codigoPostal2 = 4470;
        Reparticao r2 = new Reparticao(cidade2, nrReparticao2, codigoPostal2);
        Servico s3 = new Servico('A');
        Servico s4 = new Servico('B');
        r2.getServicos().getServicos().add(s3);
        r2.getServicos().getServicos().add(s4);

        rp.getReparticoes().addLast(r2);

        String nome1 = "Ana";
        String nrContribuinte1 = "111222333";
        String email1 = "ana@gmail.com";
        Cidadao.CodigoPostal codigoPostalCidadao1 = new Cidadao.CodigoPostal(4200, "072");
        int nrReparticaoCidadao1 = 1234;
        Cidadao c1 = new Cidadao(nrContribuinte1, nome1, email1, codigoPostalCidadao1, nrReparticaoCidadao1);

        String nome2 = "João";
        String nrContribuinte2 = "222222222";
        String email2 = "joao@gmail.com";
        Cidadao.CodigoPostal codigoPostalCidadao2 = new Cidadao.CodigoPostal(4300, "072");
        int nrReparticaoCidadao2 = 1234;
        Cidadao c2 = new Cidadao(nrContribuinte2, nome2, email2, codigoPostalCidadao2, nrReparticaoCidadao2);

        s1.getFila().addLast(c1);
        s1.getFila().addLast(null);
        s1.getFila().addLast(c2);
        s2.getFila().addLast(c2);
        s3.getFila().addLast(c1);
        s4.getFila().addLast(c2);

        Map<Servico, Integer> expected = new TreeMap<>();

        expected.put(new Servico('A'), 3);
        expected.put(new Servico('B'), 2);

        Map<Servico, Integer> result = rp.servicosComMaiorProcura();

        assertEquals(expected, result);

    }

    @Test
    public void ensureCidadaosAtendidosIsWorking() {
        RegistoReparticao rp = new RegistoReparticao();

        String cidade = "Porto";
        int nrReparticao = 1234;
        int codigoPostal = 4200;
        Reparticao r = new Reparticao(cidade, nrReparticao, codigoPostal);
        Servico s1 = new Servico('A');
        Servico s2 = new Servico('B');
        Servico s3 = new Servico('C');
        r.getServicos().getServicos().add(s1);
        r.getServicos().getServicos().add(s2);
        r.getServicos().getServicos().add(s3);

        rp.getReparticoes().addLast(r);

        List<Cidadao> cidadaos = new ArrayList<>();
        for (int i = 1; i < 4; i++) {
            cidadaos.add(new Cidadao(String.valueOf(i), "nome" + i, "email" + i + "@gmail.com", new Cidadao.CodigoPostal(1000 + i, "100"), nrReparticao));
        }

        s1.getFila().addLast(cidadaos.get(0));
        s1.getFila().addLast(null);
        s1.getFila().addLast(cidadaos.get(2));

        s2.getFila().addLast(cidadaos.get(2));
        s2.getFila().addLast(cidadaos.get(1));
        s2.getFila().addLast(null);

        s3.getFila().addLast(cidadaos.get(1));
        s3.getFila().addLast(cidadaos.get(2));
        s3.getFila().addLast(cidadaos.get(0));

        DoublyLinkedList<Cidadao> atendidosServicoA = new DoublyLinkedList<>();
        DoublyLinkedList<Cidadao> atendidosServicoB = new DoublyLinkedList<>();
        DoublyLinkedList<Cidadao> atendidosServicoC = new DoublyLinkedList<>();

        atendidosServicoA.addLast(cidadaos.get(0));

        atendidosServicoB.addLast(cidadaos.get(2));
        atendidosServicoB.addLast(cidadaos.get(1));

        atendidosServicoC.addLast(cidadaos.get(1));
        atendidosServicoC.addLast(cidadaos.get(2));

        Map<Servico, DoublyLinkedList<Cidadao>> expected = new TreeMap<>();
        expected.put(s1, atendidosServicoA);
        expected.put(s2, atendidosServicoB);
        expected.put(s3, atendidosServicoC);
        Map<Servico, DoublyLinkedList<Cidadao>> result = rp.cidadaosAtendidos(r, new Hora(9, 25));

        assertEquals(expected, result);
    }

    @Test
    public void ensureCidadaosAtendidosInvalidHourReturnsNull() {
        RegistoReparticao rp = new RegistoReparticao();

        String cidade = "Porto";
        int nrReparticao = 1234;
        int codigoPostal = 4200;
        Reparticao r = new Reparticao(cidade, nrReparticao, codigoPostal);
        Servico s1 = new Servico('A');
        Servico s2 = new Servico('B');
        Servico s3 = new Servico('C');
        r.getServicos().getServicos().add(s1);
        r.getServicos().getServicos().add(s2);
        r.getServicos().getServicos().add(s3);

        rp.getReparticoes().addLast(r);

        Map<Servico, DoublyLinkedList<Cidadao>> expected = null;
        Map<Servico, DoublyLinkedList<Cidadao>> result = rp.cidadaosAtendidos(r, new Hora(8, 59));

        assertEquals(expected, result);
    }
}
