/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.Test;
import static org.junit.Assert.*;
import utils.DoublyLinkedList;

/**
 * Classe teste para a classe reparticao;
 *
 * @author Gonçalo Martins 1161544
 * @author João Dias 1160967
 */
public class ReparticaoTest {

    private static final String CIDADE = "CIDADE";
    private static final int NR_REPARTICAO = 1234;
    private static final int CODIGO_POSTAL = 4200;

    @Test
    public void ensureSameObjectIsEqual() {

        Reparticao expected = new Reparticao(CIDADE, NR_REPARTICAO, CODIGO_POSTAL);

        assertEquals(expected, expected);
    }

    @Test
    public void ensureDifferentObjectAreNotEqual() {

        Reparticao expected = new Reparticao(CIDADE, NR_REPARTICAO, CODIGO_POSTAL);

        Object result = new Object();

        assertNotEquals(expected, result);
    }

    @Test
    public void ensureSameNrReparticaoDifferentCodigoPostalObjectsAreNotEqual() {

        Reparticao expected = new Reparticao(CIDADE, NR_REPARTICAO, CODIGO_POSTAL);

        int codigoPostal2 = 4300;
        Reparticao result = new Reparticao(CIDADE, NR_REPARTICAO, codigoPostal2);

        assertNotEquals(expected, result);
    }

    @Test
    public void ensureSameCodigoPostalODifferentNrReparticaObjectsAreNotEqual() {

        Reparticao expected = new Reparticao(CIDADE, NR_REPARTICAO, CODIGO_POSTAL);

        int nrReparticao2 = 1235;
        Reparticao result = new Reparticao(CIDADE, nrReparticao2, CODIGO_POSTAL);

        assertNotEquals(expected, result);
    }

    @Test
    public void ensureSameNrReparticaoSameCodigoPostalObjectsAreEqual() {

        Reparticao expected = new Reparticao(CIDADE, NR_REPARTICAO, CODIGO_POSTAL);

        Reparticao result = new Reparticao(CIDADE, NR_REPARTICAO, CODIGO_POSTAL);

        assertEquals(expected, result);
    }

    @Test
    public void ensureDifferentNrContribuinteAndCodigoPostalObjectsAreNotEqual() {

        Reparticao expected = new Reparticao(CIDADE, NR_REPARTICAO, CODIGO_POSTAL);

        int nrReparticao2 = 1235;
        int codigoPostal2 = 4300;
        Reparticao result = new Reparticao(CIDADE, nrReparticao2, codigoPostal2);

        assertNotEquals(expected, result);
    }

    @Test
    public void ensureHashCodeIsCorrect() {

        Reparticao r = new Reparticao(CIDADE, NR_REPARTICAO, CODIGO_POSTAL);

        int expected = NR_REPARTICAO + CODIGO_POSTAL;
        int result = r.hashCode();

        assertEquals(expected, result);
    }
    
       @Test
    public void ensureAbandonarFilasIsWorking() {
        String cidade = "Porto";
        int nrReparticao = 1234;
        int codigoPostal = 4200;
        Reparticao r = new Reparticao(cidade, nrReparticao, codigoPostal);
        Servico s1 = new Servico('A');
        r.getServicos().getServicos().add(s1);
        
        String nome1 = "Ana";
        String nrContribuinte1 = "111222333";
        String email1 = "ana@gmail.com";
        Cidadao.CodigoPostal codigoPostalCidadao1 = new Cidadao.CodigoPostal(4200, "072");
        int nrReparticaoCidadao1 = 1234;
        Cidadao c = new Cidadao(nrContribuinte1, nome1, email1, codigoPostalCidadao1, nrReparticaoCidadao1);
        r.getListaCidadao().getCidadaos().add(c);
        s1.getFila().addLast(c);
        
        r.abandonarFilas(c);
        
        DoublyLinkedList<Cidadao> expected = new DoublyLinkedList<>();
        expected.addLast(null);
        DoublyLinkedList<Cidadao> result = s1.getFila();
        assertEquals(expected, result);
    }

}
