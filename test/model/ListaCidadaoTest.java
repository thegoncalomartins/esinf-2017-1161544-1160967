/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashSet;
import java.util.Set;
import model.Cidadao.CodigoPostal;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Classe teste para a classe ListaCidadao.
 *
 * @author Gonçalo Martins 1161544
 * @author João Dias 1160967
 */
public class ListaCidadaoTest {

    private static final String NR_CONTRIBUINTE_1 = "999999999";
    private static final String NR_CONTRIBUINTE_2 = "111111111";
    private static final String NOME_1 = "NOME1";
    private static final String NOME_2 = "NOME2";
    private static final String EMAIL_1 = "email1@email.com";
    private static final String EMAIL_2 = "email2@email.com";
    private static final CodigoPostal COD_POSTAL_1 = new CodigoPostal(4475, "217");
    private static final CodigoPostal COD_POSTAL_2 = new CodigoPostal(4300, "100");
    private static final int NR_REPARTICAO_1 = 1234;
    private static final int NR_REPARTICAO_2 = 4321;

    @Test
    public void ensureRemoverCidadaosPorCodigoPostalReturnsRemovedCidadao() {
        ListaCidadao lc = new ListaCidadao();

        Cidadao c1 = new Cidadao(NR_CONTRIBUINTE_1, NOME_1, EMAIL_1, COD_POSTAL_1, NR_REPARTICAO_1);
        Cidadao c2 = new Cidadao(NR_CONTRIBUINTE_2, NOME_2, EMAIL_2, COD_POSTAL_2, NR_REPARTICAO_2);

        lc.getCidadaos().add(c1);
        lc.getCidadaos().add(c2);

        Set<Cidadao> expected = new HashSet<>();
        expected.add(c1);
        Set<Cidadao> result = lc.removerCidadaosPorCodigoPostal(COD_POSTAL_1.getRegiao());

        assertEquals(expected, result);
    }

    @Test
    public void ensureRemoverCidadaosPorCodigoPostalRemovesCidadao() {
        ListaCidadao lc = new ListaCidadao();
        Cidadao c1 = new Cidadao(NR_CONTRIBUINTE_1, NOME_1, EMAIL_1, COD_POSTAL_1, NR_REPARTICAO_1);
        Cidadao c2 = new Cidadao(NR_CONTRIBUINTE_2, NOME_2, EMAIL_2, COD_POSTAL_2, NR_REPARTICAO_2);
        lc.getCidadaos().add(c1);
        lc.getCidadaos().add(c2);

        lc.removerCidadaosPorCodigoPostal(COD_POSTAL_2.getRegiao());

        Set<Cidadao> expected = new HashSet<>();
        expected.add(c1);
        Set<Cidadao> result = lc.getCidadaos();
        assertEquals(expected, result);
    }

//    @Test
//    public void ensureAbandorFilaIsWorking() {
//        String cidade = "Porto";
//        int nrReparticao = 1234;
//        int codigoPostal = 4200;
//        Reparticao r = new Reparticao(cidade, nrReparticao, codigoPostal);
//        r.getServicos().getServicos().add('A');
//        
//        String nrContribuinte = "999999999";
//        String nome = "nome";
//        String email = "email@email.com";
//        CodigoPostal codigoPostalCidadao = new CodigoPostal(1111, "111");
//        int nrReparticaoCidadao = 1234;
//        
//        Cidadao c = new Cidadao(nrContribuinte, nome, email, codigoPostalCidadao, nrReparticaoCidadao);
//        r.getListaCidadao().getCidadaos().put(c, new TreeSet<>());
//        
//        Senha s = new Senha(1, 'A');
//        r.getListaCidadao().getCidadaos().get(c).add(s);
//        
//        r.getListaCidadao().abandonarFila(c);
//        
//        Set<Senha> expected = new TreeSet<>();
//        Set<Senha> result = r.getListaCidadao().getCidadaos().get(c);
//        assertEquals(expected, result);
//    }

}
