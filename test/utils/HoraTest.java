/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gmartins97
 */
public class HoraTest {

    @Test
    public void ensureHashCodeMethodIsWorking() {
        int hora = 10;
        int min = 30;
        Hora tempo = new Hora(hora, min);
        int expected = hora + min;

        int result = tempo.hashCode();

        assertEquals(expected, result);
    }

    @Test
    public void ensureBiggerHourReturnsTrue() {
        Hora hora = new Hora(15, 30);

        Hora outra = new Hora(14, 40);

        assertTrue(hora.isMaiorOuIgual(outra));
    }

    @Test
    public void ensureSmallerHourReturnsFalse() {
        Hora hora = new Hora(10, 30);

        Hora outra = new Hora(14, 40);

        assertFalse(hora.isMaiorOuIgual(outra));
    }

    @Test
    public void ensureSameHourAndMoreMinutesReturnsTrue() {
        Hora hora = new Hora(15, 30);

        Hora outra = new Hora(15, 10);

        assertTrue(hora.isMaiorOuIgual(outra));
    }

    @Test
    public void ensureSameHourAndLessMinutesReturnsFalse() {
        Hora hora = new Hora(15, 10);

        Hora outra = new Hora(15, 30);

        assertFalse(hora.isMaiorOuIgual(outra));
    }

    @Test
    public void ensureSameHourAndSameMinutesReturnsTrue() {
        Hora hora = new Hora(15, 30);

        Hora outra = new Hora(15, 30);

        assertTrue(hora.isMaiorOuIgual(outra));
    }

}
