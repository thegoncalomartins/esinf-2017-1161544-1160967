/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.HashSet;
import java.util.Set;
import model.Cidadao;
import model.Cidadao.CodigoPostal;
import model.RegistoReparticao;
import model.Reparticao;
import model.Servico;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author m
 */
public class LeituraFicheirosTest {

    @Test
    public void ensureLerReparticoesAddsReparticoes() {
        RegistoReparticao rp = new RegistoReparticao();
        String nomeFich = "fx_reparticoes_teste.txt";

        String cidade1 = "Porto";
        int nrReparticao1 = 1234;
        int codigoPostal1 = 4200;
        Reparticao r1 = new Reparticao(cidade1, nrReparticao1, codigoPostal1);
        Servico s1 = new Servico('A');
        Servico s2 = new Servico('C');
        Servico s3 = new Servico('D');
        r1.getServicos().getServicos().add(s1);
        r1.getServicos().getServicos().add(s2);
        r1.getServicos().getServicos().add(s3);

        String cidade2 = "Maia";
        int nrReparticao2 = 1235;
        int codigoPostal2 = 4470;
        Reparticao r2 = new Reparticao(cidade2, nrReparticao2, codigoPostal2);
        Servico s4 = new Servico('A');
        Servico s5 = new Servico('B');
        r2.getServicos().getServicos().add(s4);
        r2.getServicos().getServicos().add(s5);

        LeituraFicheiros.lerReparticoes(nomeFich, rp);

        DoublyLinkedList<Reparticao> expected = new DoublyLinkedList<>();
        expected.addLast(r1);
        expected.addLast(r2);
        DoublyLinkedList<Reparticao> result = rp.getReparticoes();
        assertEquals(expected, result);
    }

    @Test
    public void ensureLerCidadaosAddsCidadaos() {
        RegistoReparticao rp = new RegistoReparticao();
        String nomeFich = "fx_cidadaos_teste.txt";

        String cidade = "Porto";
        int nrReparticao = 1234;
        int codigoPostal = 4200;
        Reparticao r = new Reparticao(cidade, nrReparticao, codigoPostal);
        Servico s1 = new Servico('A');
        Servico s2 = new Servico('C');
        Servico s3 = new Servico('D');
        r.getServicos().getServicos().add(s1);
        r.getServicos().getServicos().add(s2);
        r.getServicos().getServicos().add(s3);

        rp.getReparticoes().addLast(r);

        String nome1 = "Ana";
        String nrContribuinte1 = "111222333";
        String email1 = "ana@gmail.com";
        CodigoPostal codigoPostal1 = new CodigoPostal(4200, "072");
        int nrReparticaoCidadao1 = 1234;
        Cidadao c1 = new Cidadao(nrContribuinte1, nome1, email1, codigoPostal1, nrReparticaoCidadao1);

        String nome2 = "Berta";
        String nrContribuinte2 = "223344";
        String email2 = "berta@gmail.com";
        CodigoPostal codigoPostal2 = new CodigoPostal(4200, "071");
        int nrReparticaoCidadao2 = 1234;
        Cidadao c2 = new Cidadao(nrContribuinte2, nome2, email2, codigoPostal2, nrReparticaoCidadao2);

        LeituraFicheiros.lerCidadaos(nomeFich, rp);

        Set<Cidadao> expected = new HashSet<>();
        expected.add(c1);
        expected.add(c2);
        Set<Cidadao> result = r.getListaCidadao().getCidadaos();
        assertEquals(expected, result);
    }

//    @Test
//    public void ensureLerSenhasAddsSenhas() {
//        RegistoReparticao rp = new RegistoReparticao();
//        String nomeFich = "fx_senhas_teste.txt";
//        
//        String cidade = "Porto";
//        int nrReparticao = 1234;
//        int codigoPostal = 4200;
//        Reparticao r = new Reparticao(cidade, nrReparticao, codigoPostal);
//        r.getServicos().getServicos().add('A');
//        r.getServicos().getServicos().add('C');
//        r.getServicos().getServicos().add('D');
//        
//        String nome = "Ana";
//        String nrContribuinte = "111222333";
//        String email = "ana@gmail.com";
//        CodigoPostal codPostal = new CodigoPostal(4200, "072");
//        int nrReparticaoCidadao = 1234;
//        Cidadao c = new Cidadao(nrContribuinte, nome, email, codPostal, nrReparticaoCidadao);
//        
//        r.getListaCidadao().getCidadaos().put(c, new TreeSet<>());
//        rp.getReparticoes().addLast(r);
//        
//        int nrOrdem = 1;
//        char servico1 = 'A';
//        char servico2 = 'C';
//        
//        Senha senha1 = new Senha(nrOrdem, servico1);
//        Senha senha2 = new Senha(nrOrdem, servico2);
//        
//        LeituraFicheiros.lerSenhas(nomeFich, rp);
//        
//        Set<Senha> expected = new TreeSet<>();
//        expected.add(senha1);
//        expected.add(senha2);
//        Set<Senha> result = r.getListaCidadao().getCidadaos().get(c);
//        assertEquals(expected, result);
//    }
    @Test
    public void ensureLerSenhasAddsCidadao() {
        RegistoReparticao rp = new RegistoReparticao();
        String nomeFich = "fx_senhas_teste.txt";

        String cidade = "Porto";
        int nrReparticao = 1234;
        int codigoPostal = 4200;
        Reparticao r = new Reparticao(cidade, nrReparticao, codigoPostal);
        Servico s1 = new Servico('A');
        Servico s2 = new Servico('C');
        Servico s3 = new Servico('D');
        r.getServicos().getServicos().add(s1);
        r.getServicos().getServicos().add(s2);
        r.getServicos().getServicos().add(s3);

        rp.getReparticoes().addLast(r);

        String nome1 = "Ana";
        String nrContribuinte1 = "111222333";
        String email1 = "ana@gmail.com";
        CodigoPostal codigoPostal1 = new CodigoPostal(4200, "072");
        int nrReparticaoCidadao1 = 1234;
        Cidadao c1 = new Cidadao(nrContribuinte1, nome1, email1, codigoPostal1, nrReparticaoCidadao1);

        r.getListaCidadao().getCidadaos().add(c1);

        LeituraFicheiros.lerSenhas(nomeFich, rp);

        DoublyLinkedList<Cidadao> expected = new DoublyLinkedList<>();
        expected.addLast(c1);
        DoublyLinkedList<Cidadao> result = r.getServicos().obterFilaPorCodigoDeServico('A');
        assertEquals(expected, result);
    }

    @Test
    public void ensureLerSenhasAddsCidadaoInCorrectPosition() {
        RegistoReparticao rp = new RegistoReparticao();
        String nomeFich = "fx_senhas_teste.txt";

        String cidade = "Porto";
        int nrReparticao = 1234;
        int codigoPostal = 4200;
        Reparticao r = new Reparticao(cidade, nrReparticao, codigoPostal);
        Servico s1 = new Servico('A');
        Servico s2 = new Servico('C');
        Servico s3 = new Servico('D');
        r.getServicos().getServicos().add(s1);
        r.getServicos().getServicos().add(s2);
        r.getServicos().getServicos().add(s3);

        rp.getReparticoes().addLast(r);

        String nome1 = "Ana";
        String nrContribuinte1 = "111222333";
        String email1 = "ana@gmail.com";
        CodigoPostal codigoPostal1 = new CodigoPostal(4200, "072");
        int nrReparticaoCidadao1 = 1234;
        Cidadao c1 = new Cidadao(nrContribuinte1, nome1, email1, codigoPostal1, nrReparticaoCidadao1);

        r.getListaCidadao().getCidadaos().add(c1);

        LeituraFicheiros.lerSenhas(nomeFich, rp);

        DoublyLinkedList<Cidadao> expected = new DoublyLinkedList<>();
        expected.addLast(null);
        expected.addLast(c1);
        
        DoublyLinkedList<Cidadao> result = r.getServicos().obterFilaPorCodigoDeServico('C');
        assertEquals(expected, result);
    }
    
    @Test
    public void ensureLerSenhasWorksAsIntended() {
        RegistoReparticao rp = new RegistoReparticao();
        String nomeFich = "fx_senhas_teste_2.txt";

        String cidade = "Porto";
        int nrReparticao = 1234;
        int codigoPostal = 4200;
        Reparticao r = new Reparticao(cidade, nrReparticao, codigoPostal);
        Servico s1 = new Servico('A');
        Servico s2 = new Servico('C');
        r.getServicos().getServicos().add(s1);
        r.getServicos().getServicos().add(s2);

        rp.getReparticoes().addLast(r);

        String nome1 = "Ana";
        String nrContribuinte1 = "111222333";
        String email1 = "ana@gmail.com";
        CodigoPostal codigoPostal1 = new CodigoPostal(4200, "072");
        int nrReparticaoCidadao1 = 1234;
        Cidadao c1 = new Cidadao(nrContribuinte1, nome1, email1, codigoPostal1, nrReparticaoCidadao1);
        r.getListaCidadao().getCidadaos().add(c1);
        
        String nome2 = "João";
        String nrContribuinte2 = "222222222";
        String email2 = "joao@gmail.com";
        CodigoPostal codigoPostal2 = new CodigoPostal(4300, "072");
        int nrReparticaoCidadao2 = 1234;
        Cidadao c2 = new Cidadao(nrContribuinte2, nome2, email2, codigoPostal2, nrReparticaoCidadao2);
        r.getListaCidadao().getCidadaos().add(c2);
        
        String nome3 = "Terceiro";
        String nrContribuinte3 = "333333333";
        String email3 = "terceiro@gmail.com";
        CodigoPostal codigoPostal3 = new CodigoPostal(4400, "073");
        int nrReparticaoCidadao3 = 1334;
        Cidadao c3 = new Cidadao(nrContribuinte3, nome3, email3, codigoPostal3, nrReparticaoCidadao3);
        r.getListaCidadao().getCidadaos().add(c3);
        
        LeituraFicheiros.lerSenhas(nomeFich, rp);
        
        DoublyLinkedList<Cidadao> expected1 = new DoublyLinkedList<>();
        expected1.addLast(c1);
        expected1.addLast(c2);
        DoublyLinkedList<Cidadao> result1 = r.getServicos().obterFilaPorCodigoDeServico('A');
        assertEquals(expected1, result1);
        
        DoublyLinkedList<Cidadao> expected2 = new DoublyLinkedList<>();
        expected2.addLast(c3);
        expected2.addLast(c1);
        DoublyLinkedList<Cidadao> result2 = r.getServicos().obterFilaPorCodigoDeServico('C');
        assertEquals(expected2, result2);
    }

}
